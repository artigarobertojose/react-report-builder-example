# React Report Builder example

This project is a usage example of [react-report-builder](https://www.npmjs.com/package/react-report-builder)

TypeScript example code can be found at [`app/index.tsx`](app/index.tsx). To run it you need to:

- Clone this repository
- Install dependencies by running `npm install`
- Execute command `npm run start` (it builds the example and runs it at http://localhost:3000/)

